from distutils.core import setup

setup(name="PrivacyScoreAppMetrics",
      version="1.0.0",
      description="implements geo data structures and algorithms for analysing privacy in geo data",
      author="Erik Rötschke",
      authoer_email="info@erik-roetschke.de",
      url='https://git.informatik.uni-leipzig.de/scads/de4l/privacy/PrivacyScoreAppMetrics.git',
      packages=['PrivacyScoreAppMetrics']
)
